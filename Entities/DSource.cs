﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Event.Domain.Entities
{
    public class DSource
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Guid? EventId { get; set; }
        [ForeignKey(nameof(EventId))]
        public virtual DEvent Event { get; set; }
    }
}
